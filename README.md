This unnamed wonderful sleep-deprived built project watches for pushes on a branch of a given repository and then loops over a set of repositories from a YAML file and requests builds from Travis CI.
It's primary usage is to generate new builds when I change the documentation template so I don't have to manually worry about building a large number of projects.

[![forthebadge](http://forthebadge.com/images/badges/as-seen-on-tv.svg)](http://forthebadge.com)
